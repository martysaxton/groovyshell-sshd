/*******************************************************************************
 * Copyright 2011 Marty Saxton
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsshd;
import groovy.io.GroovyPrintStream;
import groovy.lang.Binding;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Callable;

import org.apache.sshd.server.Command;
import org.apache.sshd.server.Environment;
import org.apache.sshd.server.ExitCallback;
import org.codehaus.groovy.tools.shell.Groovysh;
import org.codehaus.groovy.tools.shell.IO;
import org.fusesource.jansi.Ansi;

/**
 * Integrates the Groovy Shell with Apache SSHD
 *
 */
public class GroovySecureShellServer implements Command {

	static {
		
    	// tell jline to use our Terminal implementation so it doesn't default to the one for the host system's platform
    	System.setProperty("jline.terminal", PesudoTerminal.class.getName());

    	// force jansi to use color, regardless of host operating system
    	Ansi.setDetector(new Callable<Boolean>() {
			public Boolean call() throws Exception {
				return true;
			}
		});

	}
	
	/** SshServer's supplied input stream */
	private InputStream instr;
	
	/** SshServer's supplied output stream */
	private OutputStream outstr;

	/** SshServer's supplied error stream */
	private OutputStream errstr;
	
	/** exit callback used to tell the ssh server that an instance wants to exit and disconnect */
	private ExitCallback exitCallback;
	
	/** variable bindings for the shell instance */
	Binding binding;
	
	public void setInputStream(InputStream in) {
		this.instr = in;
	}

	public void setOutputStream(OutputStream out) {
		this.outstr = out;
	}

	public void setErrorStream(OutputStream err) {
		this.errstr = err;
	}

	public void setExitCallback(ExitCallback callback) {
		this.exitCallback = callback;
	}

	public void start(Environment env) throws IOException {
        
		// start a separate thread
		Thread thread = new Thread("groovysh-server") {
        	
            @Override public void run() {
            	
            	// start groovy shell using the SshServer's supplied output and error streams 
            	// by puting them in the binding using the special names 'out' and 'err' 
            	IO io = new IO(instr, outstr, errstr);
            	if (binding == null) {
            		binding = new Binding();
            	}
            	binding.setVariable("out", new GroovyPrintStream(outstr));
            	binding.setVariable("err", new GroovyPrintStream(errstr));
            	Groovysh groovysh =  new Groovysh(binding, io);
            	groovysh.run((String)null);
            	
            	// back from groovysh because the shell has terminated, tell the ssh server to exit
            	exitCallback.onExit(0);
            }
        };
        thread.start();
	}

	public void destroy() {
		// no op
	}

}
