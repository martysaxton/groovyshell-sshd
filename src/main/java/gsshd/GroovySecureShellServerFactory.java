/*******************************************************************************
 * Copyright 2011 Marty Saxton
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsshd;


import groovy.lang.Binding;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.sshd.SshServer;
import org.apache.sshd.common.Factory;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.PasswordAuthenticator;
import org.apache.sshd.server.ServerFactoryManager;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.session.ServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Companion factory class to integrate with apache sshd.
 */
public class GroovySecureShellServerFactory implements Factory<Command> {
	
	private static Logger logger = LoggerFactory.getLogger(GroovySecureShellServerFactory.class);
	
	/** application specific variables to place into the binding of each shell instance */
	private Map<String, Object> variables = new HashMap<String, Object>();
	
	/** port that the ssh sever should listen to */
	private int port = 2222;
	
	/** inactivity timeout for the ssh server */
	private int timeOutMiliseconds = Integer.MAX_VALUE;

	private PasswordAuthenticator passwordAuthenticator;
	
	public Map<String, Object> variables() {
		return variables;
	}
	
	@Override
	public Command create() {
		
		GroovySecureShellServer gsshd = new GroovySecureShellServer();
		
		// create a binding for the shell instance and populate it with user supplied variables 
		gsshd.binding = new Binding();
		for (Entry<String, Object> entry : variables.entrySet()) {
			gsshd.binding.setVariable(entry.getKey(), entry.getValue());
		}
		
		return gsshd;
	}

	/**
	 * Supply an alternate port for the groovysh-server's sshd to listen to.
	 * The default port is 2222.
	 * @param port the port that the ssh server should listen to
	 */
	public void setPort(int port) {
		this.port = port;
	}
	
	/**
	 * 
	 * @param timeOutMiliseconds
	 */
	public void setIdleTimeout(int timeOutMiliseconds) {
		this.timeOutMiliseconds = timeOutMiliseconds;
	}

	/**
	 * Provide an alternate password authenticator for the sshd.  
	 * If a PasswordAuthenticator is not provided then DemoPasswordAuthenticator will be used by default.
	 *  
	 * @param passwordAuthenticator the PasswordAuthenticator to be used by the sshd.
	 */
	public void setPasswordAuthenticator(PasswordAuthenticator passwordAuthenticator) {
		this.passwordAuthenticator = passwordAuthenticator;
	}

	public void start() {
		try {
			
			SshServer sshd = SshServer.setUpDefaultServer();

			// set the config directory for the sshd 
			File configDir = gsshdConfigDir();
			if (!configDir.mkdirs() && !configDir.isDirectory()) {
				throw new RuntimeException("cannot create configuration directory: " + configDir.getAbsolutePath());
			}
			
			// init the sshd with various things
			sshd.setKeyPairProvider(new SimpleGeneratorHostKeyProvider(new File(configDir, "host-key").getAbsolutePath()));
			sshd.setPort(port);
			sshd.setShellFactory(this);
	        sshd.setPasswordAuthenticator(getPasswordAuthenticator());
	        sshd.getProperties().put(ServerFactoryManager.IDLE_TIMEOUT, Integer.toString(timeOutMiliseconds));
	        
	        // start the sshd
			sshd.start();
			logger.info("gsshd listening to port {}", port);
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	
	private static File gsshdConfigDir() {
		String path = System.getProperty("user.home") + File.separator + ".gssh";
		return new File(path);
	}

	private PasswordAuthenticator getPasswordAuthenticator() {
		if (passwordAuthenticator == null) {
			logger.warn("Using DemoPasswordAuthenticator for ssh authentication.  This provides no actual security and should not be used for prouction.");
			passwordAuthenticator = new DemoPasswordAuthenticator();
		}
		return passwordAuthenticator;
	}

	/**
	 * Password authenticator that uses the username for the password.
	 * This implementation is for demonstration purposes only and is not intended for production. 
	 */
	private static class DemoPasswordAuthenticator implements PasswordAuthenticator {
		
		@Override
        public boolean authenticate(String username, String password, ServerSession session) {
            return username != null && username.equals(password);
        }
        
	}

}
